# C语言
### 一、函数

1 [printf](https://gitee.com/qq97950127/MyKnowledge/blob/master/C语言/1、函数/printf.md)

2 scanf()

3 getchar()\putchar()


### 二、数据类型

1 浮点数

2 char类型

3、结构体/位域


### 三、指针、数组


### 四、排序算法


### 五、函数


### 六、关键字/标识符


### 七、进制转换


### 八、运算符与表达式


### 九、内存





 

# Linux

### 一、SHELL基本命令



 

# 其他

### 一、编译系统


### 二、ASCII码

 

# 文献资料

### 1、C语言
[C语言深度解剖](https://gitee.com/qq97950127/MyKnowledge/blob/master/文献资料/C语言深度剖析.pdf)

[C语言指针经验总结(经典)](https://gitee.com/qq97950127/MyKnowledge/blob/master/文献资料/C语言指针经验总结(经典).pdf)

### 2、Linux
[华为的内部linux教程](https://gitee.com/qq97950127/MyKnowledge/blob/master/文献资料/华为的内部linux教程.pdf)

### 3、其他

